import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;



public class StringTest {

	public static void main(String[] args) {
 
		/*
		 * for (int i = 0; i < 100; i++) {
		 * System.out.println("String[] s"+i+"={\"\",\"\"}; sL.add(s"+i+");"); }
		 */

		ArrayList<String> sList = StringKutuphane.stringTopla();
		HashMap<String, ArrayList<String>> map = StringKutuphane.getKelimeler();
		
		for (Iterator iterator = sList.iterator(); iterator.hasNext();) {
			String s = (String) iterator.next();
			System.out.println("\t--------------------------------------------------------------------------------------------");
			System.out.println(s+"\n");
			String kontrol=s.replaceAll("\\s+", "").trim();
			kontrol=kontrol.replaceAll("\\P{Alnum}", "");
			kontrol=kontrol.replaceAll("[0-9]","");
		//	System.out.println(kontrol1);
		//	System.out.println(buyukHarfKontrol? "busbuyuk":"b degil");
			if (StringUtils.isAllUpperCase(kontrol)) {
				s=s.replace("I", "ı");
				char[] charArray = {'.'};
				s=WordUtils.capitalizeFully(s,charArray);
			}
			
			degisimYap(s,map); 

		}

	}

	private static void degisimYap(String s, HashMap<String,ArrayList<String>> map) {
		Random r = new Random();
		for (Entry entry : map.entrySet()) {
			s=s+" ";
			String key = (String) entry.getKey();
			ArrayList<String> list = (ArrayList<String>) entry.getValue();
			String kucukHarfli=s.toLowerCase();
			if (kucukHarfli.contains(key)) {
				int ilk=kucukHarfli.indexOf(key);
				int son=ilk+key.length();
				String eskiKelime=s.substring(ilk, son);
				String yeniKelime=list.get(r.nextInt(list.size()));
				String eskiKelimeDevami=s.substring(son, son+1);
				String eskiKelimeOncesi=ilk!=0 ? eskiKelimeOncesi=s.substring(ilk-1, ilk):null;
				if (!Character.isLetter(eskiKelimeDevami.toCharArray()[0])&& eskiKelimeOncesi!=null && !Character.isLetter(eskiKelimeOncesi.toCharArray()[0])) {
					if (Character.isUpperCase(eskiKelime.charAt(0))) {
							yeniKelime = yeniKelime.substring(0,1).toUpperCase() + yeniKelime.substring(1);
							}
				s=s.replace(eskiKelime, yeniKelime);
				//s=s.substring(0, ilk)+yeniKelime+s.substring(son, s.length());
				//s = s.replace(key, yeniKelime);
				}
			//	System.out.println(s+"   "+key+" ++ "+eskiKelime+"-->"+yeniKelime+" __"+eskiKelimeDevami+"__");
			}

		}
		s=s.substring(0, s.length()-1);
	//	System.out.println(s);
		s=s.replaceAll("\\s+", " ").trim();
		System.out.println(s);
	}
	
}
