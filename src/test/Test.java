package test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class Test {
	
	
	
	
	
	
	
	public static void main(String[] args) {
		
		String gelenString="Etkilenen Bölgeler : Gençosman Mahallesinin;Ayazma Caddesi,Ayaz,Meslek Sokakları,Esnaf Sitesi ve civarı Yatırım Çalışması İli : İstanbul "
				+ "en Etkilenen Yerler : Tahtakale Mahallesinin;Tahtakale Çıkmazı ile Tomruk,Tomruk Çıkmazı ve civarı Bakım Onarım Çalışması"
				+ "İlçe : Başakşehir Etkilenen Yerler : şahntepe mh"
				+ "Etkilenen Bölgeler : esentepe mh, deneyis mah,yunusemre mah,ulubatlı hasan mah"
				+ " Poligon Mahallesinin;Girne Caddesi Kaplıca Yolu ile Sezgin,Maraşel Fevzi,Düzgün,Erdem,Eser Sokaklar Yatırım Çalışması"
				+ "Altındağ ilçesi Başpınar mahallesi 1234/1 sokak içerisindeki şebeke arızası sebebiyle 16:00 ’a kadar su kesintisi yapılmaktadır."
				+ "Kurtuluş mahallesi Yavuzlar Caddesi içerisindeki şebeke arızası etkisiyle s"
				+ "Fatih Mahallesi, 100. yıl Mahallesi";
		
		ArrayList<String> arananList=getArananlar();
		HashSet<Object> bulunanList=new HashSet<Object>();
		
		for (Iterator iterator = arananList.iterator(); iterator.hasNext();) {
			String arananKelime = (String) iterator.next();
			Boolean buldu=false;
			System.out.println("\n\t"+arananKelime+"__________");
			
			buldu=aramaAlgoritmalari(bulunanList,arananKelime,gelenString);

			if (!buldu) {
				String sAranan=turkceKarakterleriDegistir(arananKelime);
				String sGelen=turkceKarakterleriDegistir(gelenString);
			buldu=aramaAlgoritmalari(bulunanList, sAranan,  sGelen);
			}
			if (!buldu) {
				String sAranan=arananKelime.replaceAll("\\s+","");
				String sGelen=gelenString.replaceAll("\\s+","");
			buldu=aramaAlgoritmalari(bulunanList,sAranan ,sGelen );
			} 
			
			
		}
		
		System.out.println(arananList.size()+" kelime arandı. "+bulunanList.size()+" tanesi bulundu. :"+bulunanList.toString());
	
	
	
	}

	
	
	
	private static String turkceKarakterleriDegistir(String s) {
		s=s.replace("ğ", "g");
		s=s.replace("Ğ", "G");
		s=s.replace("ş", "s");
		s=s.replace("Ş", "S");
		s=s.replace("ç", "c");
		s=s.replace("Ç", "C");
		s=s.replace("ö", "o");
		s=s.replace("Ö", "O");
		s=s.replace("ü", "u");
		s=s.replace("Ü", "U");
		s=s.replace("İ", "I");
		return s;
	}

	void deneme(){
		
	};



	private static Boolean aramaAlgoritmalari(HashSet<Object> bulunanList,
			String arananKelime, String gelenString) {

		Boolean bulundu=false;
		System.out.println("_");
		bulundu=containsIgnoreCase(gelenString, arananKelime);yaz(0,arananKelime,bulundu);
		if (!bulundu){
			bulundu=gelenString.toLowerCase().contains(arananKelime.toLowerCase());yaz(1,arananKelime,bulundu);
		} 
		if (!bulundu){
			bulundu=Pattern.compile(Pattern.quote(arananKelime), Pattern.CASE_INSENSITIVE).matcher(gelenString).find();yaz(2,arananKelime,bulundu);
		}
		if (!bulundu){
			bulundu=StringUtils.containsIgnoreCase(gelenString, arananKelime);yaz(3,arananKelime,bulundu);
		}
		if (!bulundu){
			bulundu=splitForJaro(gelenString, arananKelime);
		}
		if (bulundu) {
			bulunanList.add(arananKelime);
		}
		
		return bulundu; }




	private static Boolean splitForJaro(String gelenString, String arananKelime) {
		String[] s=gelenString.split(" ");
		Boolean jaroBuldu=false;
		System.out.println("sayı: "+s.length);
		for (int i = 0; i < s.length; i++) {
			double d = StringUtils.getJaroWinklerDistance(s[i],arananKelime);
			double sabit = 0.90;
			if (d > sabit) {
				System.out.println("jaro mahalle buldu : "+d+" "+s[i]+" - "+arananKelime+" "+i);
				jaroBuldu=true;
				break;
			}
		}
		return jaroBuldu;
	}




	private static void yaz(int i,String s,Boolean b) {
	System.out.println("girdi : "+i+" -- "+b+" "+s);
		
	}




	private static ArrayList<String> getArananlar() {
		ArrayList<String> arananList=new ArrayList<String>();
        arananList.add("BAŞPINAR");
        arananList.add("ESENTEPE");
        arananList.add("KURTULUS");
        arananList.add("TOMRUK");
        arananList.add("GENÇOSMAN");
        arananList.add("DENEYİŞ");
        arananList.add("YUNUS EMRE");
        arananList.add("ULUBATLIHASAN");
        arananList.add("100.yıl");
        arananList.add("şahintepe");
		return arananList;
	}
	
	
	public static boolean containsIgnoreCase(String src, String what) {
	    final int length = what.length();
	    if (length == 0)
	        return true; // Empty string is contained

	    final char firstLo = Character.toLowerCase(what.charAt(0));
	    final char firstUp = Character.toUpperCase(what.charAt(0));

	    for (int i = src.length() - length; i >= 0; i--) {
	        // Quick check before calling the more expensive regionMatches() method:
	        final char ch = src.charAt(i);
	        if (ch != firstLo && ch != firstUp)
	            continue;

	        if (src.regionMatches(true, i, what, 0, length))
	            return true;
	    }

	    return false;
	}

}
